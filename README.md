# [CGL](https://gitlab.com/chris-engine-opengl/cgl) & [Kiker](https://gitlab.com/chris-engine-opengl/kiker) Docs
This repository contains LaTeX source file and pdf file with documentation for CGL and Kiker game engine projects. This paper is written with a certain styling constrains because it is's bound to be published on a university.

So not to write redundantly, abstract and everything about the project is written in the document.

Both [libCGL](https://gitlab.com/chris-engine-opengl/cgl) and
[Kiker](https://gitlab.com/chris-engine-opengl/kiker) are hosted on Gitlab and are available there.
On their git pages live demos and specific information regarding them are available.